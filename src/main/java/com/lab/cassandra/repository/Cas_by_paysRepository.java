package com.lab.cassandra.repository;

import com.lab.cassandra.model.Cas_by_pays;
import com.lab.cassandra.model.Pays_by_continent;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

import java.util.List;
import java.util.UUID;

public interface Cas_by_paysRepository extends CassandraRepository<Cas_by_pays, UUID> {

    @Query("Select iso_code ,total_cases , date from cas_by_pays where iso_code = 'AFG' and date >='2021-01-01'  and date <='2021-01-31'")
    List<Cas_by_pays> casbypays();
}
