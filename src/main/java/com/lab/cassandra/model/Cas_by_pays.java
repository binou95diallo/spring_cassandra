package com.lab.cassandra.model;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.Date;

@Table
public class Cas_by_pays {
    @PrimaryKey

    private String iso_code;
    private String total_cases ;
    private Date date;

    public String getIso_code() {
        return iso_code;
    }

    public String getTotal_cases() {
        return total_cases;
    }

    public Date getDate() {
        return date;
    }
}
