package com.lab.cassandra.controller;
import com.datastax.oss.driver.api.core.uuid.Uuids;
import com.lab.cassandra.model.Lab;
import com.lab.cassandra.repository.LabRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class LabController {

    private final LabRepository labRepository;
    @Autowired
    public LabController(LabRepository labRepository) {
        this.labRepository = labRepository;
    }

    @GetMapping("/labs")
    public ResponseEntity<List<Lab>> getAllTutorials(@RequestParam(required = false) String title) {
        try {
            List<Lab> labs = new ArrayList<>();

            if (title == null)
                labRepository.findAll().forEach(labs::add);
            else
                labRepository.findByTitleContaining(title).forEach(labs::add);

            if (labs.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(labs, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/tutorials/{id}")
    public ResponseEntity<Lab> getTutorialById(@PathVariable("id") String id) {

        return null;
    }

    @PostMapping("/new_lab")
    public ResponseEntity<Lab> createTutorial(@RequestBody Lab tutorial) {
        try {
            Lab _tutorial = labRepository.save(new Lab(Uuids.timeBased(), tutorial.getTitle(), tutorial.getDescription(), false));
            return new ResponseEntity<>(_tutorial, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/tutorials/{id}")
    public ResponseEntity<Lab> updateTutorial(@PathVariable("id") String id, @RequestBody Lab tutorial) {

        return null;
    }

    @DeleteMapping("/labs/{id}")
    public ResponseEntity<HttpStatus> deleteLab(@PathVariable("id") String id) {

        return null;
    }

    @DeleteMapping("/labs")
    public ResponseEntity<HttpStatus> deleteAllLabs() {

        return null;
    }

    @GetMapping("/lab/published")
    public ResponseEntity<List<Lab>> findByPublished() {

        return null;
    }
}
