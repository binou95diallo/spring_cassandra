package com.lab.cassandra.repository;

import com.lab.cassandra.model.Continent_by_date;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface Continent_by_dateRepository extends CassandraRepository<Continent_by_date, UUID> {

    @Query("Select iso_code,continent,total_cases,new_cases," +
            "total_deaths,new_deaths from continent_by_date where date=:date  group by iso_code")
    List<Continent_by_date> contientbydate(@Param("date") LocalDate date);
}
