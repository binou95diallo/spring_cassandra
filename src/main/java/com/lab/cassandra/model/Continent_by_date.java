package com.lab.cassandra.model;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;
@Table
public class Continent_by_date {
    @PrimaryKey
    private UUID id;
    private String iso_code;
    private String continent;
    private String location;
    private String total_cases;
    private String new_cases;
    private String total_deaths;
    private String new_deaths;
    private String date;
    private String total_vaccinations;
    private String people_vaccinated;
    private String people_fully_vaccinated;
    private String new_vaccinations;
    private String total_vaccinations_per_hundred;
    private String people_vaccinated_per_hundred;
    private String people_fully_vaccinated_per_hundred;

    public String getIso_code() {
        return iso_code;
    }

    public String getContinent() {
        return continent;
    }

    public String getLocation() {
        return location;
    }

    public String getTotal_cases() {
        return total_cases;
    }

    public String getNew_cases() {
        return new_cases;
    }

    public String getTotal_deaths() {
        return total_deaths;
    }

    public String getNew_deaths() {
        return new_deaths;
    }

}
