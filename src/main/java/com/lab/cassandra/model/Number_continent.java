package com.lab.cassandra.model;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class Number_continent {
    @PrimaryKey
    private String continent;
    public String getContinent() {
        return continent;
    }

}
