package com.lab.cassandra.model;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

@Table
public class Pays_by_continent {
    @PrimaryKey

    private String iso_code;
    private String location;


    public String getIso_code() {
        return iso_code;
    }

    public String getLocation() {
        return location;
    }




}
