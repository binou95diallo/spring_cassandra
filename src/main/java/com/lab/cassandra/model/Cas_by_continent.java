package com.lab.cassandra.model;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.LocalDate;
import java.util.Date;

@Table
public class Cas_by_continent {
    @PrimaryKey

    private String continent;
    private String total;
    private LocalDate date;

    public String getContinent() {
        return continent;
    }

    public String getTotal() {
        return total;
    }

    public LocalDate getDate() {
        return date;
    }
}
