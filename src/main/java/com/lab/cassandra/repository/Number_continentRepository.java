package com.lab.cassandra.repository;

import com.lab.cassandra.model.Number_continent;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

import java.util.UUID;

public interface Number_continentRepository extends CassandraRepository<Number_continent, UUID> {

    @Query("Select count(continent) from number_continent")
    Integer numbercontinent();
}
