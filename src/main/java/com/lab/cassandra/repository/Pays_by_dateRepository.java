package com.lab.cassandra.repository;

import com.lab.cassandra.model.Pays_by_continent;
import com.lab.cassandra.model.Pays_by_date;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.UUID;

public interface Pays_by_dateRepository extends CassandraRepository<Pays_by_date, UUID> {

    @Query("SELECT iso_code , location , MAX(new_cases) as max_new_cases, date, total_vaccinations, people_vaccinated, people_fully_vaccinated, new_vaccinations, total_vaccinations_per_hundred, people_vaccinated_per_hundred, people_fully_vaccinated_per_hundred from pays_by_date where iso_code=:iso_code")
    List<Pays_by_date> paysbydate(@Param("iso_code") String iso_code);
}
