package com.lab.cassandra.repository;

import com.lab.cassandra.model.Lab;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;

import java.util.List;
import java.util.UUID;

public interface LabRepository  extends CassandraRepository<Lab, UUID> {

    @AllowFiltering
    List<Lab> findByPublished(boolean published);

    List<Lab> findByTitleContaining(String title);
}
