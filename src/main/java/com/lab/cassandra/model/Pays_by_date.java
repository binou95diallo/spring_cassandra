package com.lab.cassandra.model;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.LocalDate;

@Table
public class Pays_by_date {
    @PrimaryKey

    private String iso_code;
    private String total_cases ;
    private String location;
    private LocalDate date;
    private String new_cases;
    private String max_new_cases;
    private String total_vaccinations;
    private String people_vaccinated;
    private String people_fully_vaccinated;
    private String new_vaccinations;
    private String total_vaccinations_per_hundred;
    private String people_vaccinated_per_hundred;
    private String people_fully_vaccinated_per_hundred;

    public String getIso_code() {
        return iso_code;
    }

    public String getTotal_cases() {
        return total_cases;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getLocation() {
        return location;
    }

    public String getNew_cases() {
        return new_cases;
    }

    public String getTotal_vaccinations() {
        return total_vaccinations;
    }

    public String getPeople_vaccinated() {
        return people_vaccinated;
    }

    public String getPeople_fully_vaccinated() {
        return people_fully_vaccinated;
    }

    public String getNew_vaccinations() {
        return new_vaccinations;
    }

    public String getTotal_vaccinations_per_hundred() {
        return total_vaccinations_per_hundred;
    }

    public String getPeople_vaccinated_per_hundred() {
        return people_vaccinated_per_hundred;
    }

    public String getPeople_fully_vaccinated_per_hundred() {
        return people_fully_vaccinated_per_hundred;
    }

    public String getMax_new_cases() {
        return max_new_cases;
    }
}
