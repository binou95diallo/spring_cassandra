package com.lab.cassandra.repository;

import com.lab.cassandra.model.Pays_by_continent;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface Pays_by_continentRepository extends CassandraRepository<Pays_by_continent, UUID> {

    @Query("Select iso_code,location from pays_by_continent where continent = :continent")
    List<Pays_by_continent> paysbycontinent(@Param("continent") String continent);
}
