package com.lab.cassandra.controller;

import com.lab.cassandra.model.*;
import com.lab.cassandra.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MyAPiController {
    private final Continent_by_dateRepository continent_by_dateRepository;
    private final Pays_by_continentRepository pays_by_continentRepository;
    private final Number_continentRepository number_continentRepository;
    private final Cas_by_continentRepository cas_by_continentRepository;
    private final Cas_by_paysRepository cas_by_paysRepository;
    private final Pays_by_dateRepository pays_by_dateRepository;
    @Autowired
    public MyAPiController(Pays_by_continentRepository pays_by_continentRepository,Continent_by_dateRepository continent_by_dateRepository,Number_continentRepository number_continentRepository,
                           Cas_by_continentRepository cas_by_continentRepository,Cas_by_paysRepository cas_by_paysRepository,
                           Pays_by_dateRepository pays_by_dateRepository) {
        this.pays_by_continentRepository = pays_by_continentRepository;
        this.continent_by_dateRepository = continent_by_dateRepository;
        this.number_continentRepository = number_continentRepository;
        this.cas_by_continentRepository = cas_by_continentRepository;
        this.cas_by_paysRepository = cas_by_paysRepository;
        this.pays_by_dateRepository = pays_by_dateRepository;

    }
    //p1
    @GetMapping("/contientbydate/{date}")
    public List<Continent_by_date> getContientbydate(@PathVariable String date) throws ParseException {

        LocalDate dateFormat = LocalDate.parse(date);
        return  continent_by_dateRepository.contientbydate(dateFormat);
    }
    @GetMapping("/paysbycontinent/{continent}")
    public List<Pays_by_continent> getPaysbycontinent(@PathVariable String continent) {
        return  pays_by_continentRepository.paysbycontinent(continent);
    }
    @GetMapping("/numbercontinent")
    public Integer numbercontinent() {
        return  number_continentRepository.numbercontinent();
    }

    //p2
    @GetMapping("/paysbydate/{iso_code}")
    public List<Pays_by_date> getPpaysbydate(@PathVariable String iso_code) {
        return  pays_by_dateRepository.paysbydate(iso_code);
    }
    @GetMapping("/casbycontinent/{date}")
    public List<Cas_by_continent> getcasbycontinent(@PathVariable String date) {
        LocalDate dateFormat = LocalDate.parse(date);
        return  cas_by_continentRepository.casbycontinent(dateFormat);
    }
    @GetMapping("/casbypays")
    public List<Cas_by_pays> getCasbypays() {
        return  cas_by_paysRepository.casbypays();
    }
}
