package com.lab.cassandra.repository;

import com.lab.cassandra.model.Cas_by_continent;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface Cas_by_continentRepository extends CassandraRepository<Cas_by_continent, UUID> {

    @Query("SELECT continent ,SUM(total_cases) as total , date FROM cas_by_continent where date=:date group by continent")
    List<Cas_by_continent> casbycontinent(@Param("date") LocalDate date);
}
